package test

// Copyright (c) 2015-2019 Bitontop Technologies Inc.
// Distributed under the MIT software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.

import (
	"log"
	"testing"

	"gitlab.com/tony0408/goredmergin/coin"
	"gitlab.com/tony0408/goredmergin/exchange"
	"gitlab.com/tony0408/goredmergin/pair"

	"gitlab.com/tony0408/goredmergin/exchange/bitmex"
	"gitlab.com/tony0408/goredmergin/test/conf"
	// "../exchange/bitmex"
	// "./conf"
)

/********************Public API********************/
func Test_Bitmex(t *testing.T) {
	e := InitBitmex()

	pair := pair.GetPairByKey("USD|XBT")

	// Test_Coins(e)
	// Test_Pairs(e)
	Test_Pair(e, pair)
	// Test_Orderbook(e, pair)
	// Test_ConstraintFetch(e, pair)
	// Test_Constraint(e, pair)

	// Test_Balance(e, pair)
	// Test_Trading(e, pair, 1, 100)
	// Test_OrderStatus(e, pair, "49da8c8c-6f40-58d4-a4b8-60e7f29c285e")

	// test contractBalance
	log.Println("Test contractBalance:")
	data0 := &exchange.Contract{
		Action: exchange.CONTRACT_BALANCE, // doesn't need pair, fixed to XBt
	}
	err := e.ContractAction(data0)
	if err != nil {
		log.Printf("%v", err)
	}
	log.Printf("contractBalance Return: %+v", data0.ContractBalanceReturn) // quantity returned in real unit, not satoshi

	// // test contract buy
	// log.Println("Test contractBuy:")
	// data := &exchange.Contract{
	// 	Action:   exchange.CONTRACT_MARKET_BUY,
	// 	Pair:     pair,
	// 	Quantity: 0.01, // real quantity
	// 	Leverage: 1,
	// }
	// err = e.ContractAction(data)
	// if err != nil {
	// 	log.Printf("%s Limit Buy Err: %s", e.GetName(), err)
	// }
	// log.Printf("ContractBuy Return: %+v", data.ContractOrderReturn)

	// // test Get Addr
	// log.Println("Test Get Address:")
	// data2 := &exchange.Contract{
	// 	Action:   exchange.GET_ADDR,
	// 	Currency: pair.Base, // not necessary, fixed to XBt
	// }
	// err = e.ContractAction(data2)
	// if err != nil {
	// 	log.Printf("%s Get Addr Err: %s", e.GetName(), err)
	// }
	// log.Printf("Deposit Addr: %v", data2.Addr)

	// // test Liquidation
	// log.Println("Test Liquidation:")
	// data3 := &exchange.Contract{
	// 	Action:   exchange.LIQUIDATION,
	// 	Currency: pair.Base,
	// }
	// err = e.ContractAction(data3)
	// if err != nil {
	// 	log.Printf("%v", err)
	// }
	// log.Printf("Liqudation: %+v", data3.LiquidationOrders)

	// // use tag as 2FA token
	// Test_Withdraw_tag(e, pair.Base, 0.01, "1N3jAN7NM7YJSS7drSHoXaZoeVLpwpHmSA", "477503")

	// // test Contract OrderStatus
	// log.Println("Test ContractOrderStatus:")
	// order := &exchange.Order{
	// 	OrderID: "76f4a0d4-98e0-8b5b-a402-f433f0e8b8c2",
	// }
	// data4 := &exchange.Contract{
	// 	Action: exchange.CONTRACT_ORDER_STATUS,
	// 	Order:  order,
	// }
	// err = e.ContractAction(data4)
	// if err != nil {
	// 	log.Printf("%v", err)
	// }
	// log.Printf("ContractOrderStatus Return: %+v", data4.ContractOrderReturn)
}

func InitBitmex() exchange.Exchange {
	coin.Init()
	pair.Init()
	config := &exchange.Config{}
	config.Source = exchange.EXCHANGE_API
	conf.Exchange(exchange.BITMEX, config)

	ex := bitmex.CreateBitmex(config)
	log.Printf("Initial [ %v ] ", ex.GetName())

	config = nil
	return ex
}
