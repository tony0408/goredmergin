package huobidm

import (
	"encoding/json"
)

// Copyright (c) 2015-2019 Bitontop Technologies Inc.
// Distributed under the MIT software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.

type JsonResponse struct {
	Status string          `json:"status"`
	Data   json.RawMessage `json:"data"`
	Ts     int64           `json:"ts"`
}

type JsonResponse2 struct {
	Ch     string          `json:"ch"`
	Status string          `json:"status"`
	Tick   json.RawMessage `json:"tick"`
	Ts     int64           `json:"ts"`
}

type JsonResponse3 struct {
	Code int             `json:"code"`
	Data json.RawMessage `json:"data"`
}

/********** Public API Structure**********/
type ContractsData []struct {
	Symbol         string  `json:"symbol"`
	ContractCode   string  `json:"contract_code"`
	ContractType   string  `json:"contract_type"`
	ContractSize   float64 `json:"contract_size"`
	PriceTick      float64 `json:"price_tick"`
	DeliveryDate   string  `json:"delivery_date"`
	CreateDate     string  `json:"create_date"`
	ContractStatus int     `json:"contract_status"`
}

type PairsData []struct {
	Symbol      string  `json:"symbol"`
	Status      string  `json:"status"`
	BaseAsset   string  `json:"baseAsset"`
	QuoteAsset  string  `json:"quoteAsset"`
	MakerFee    float64 `json:"makerFee"`
	TakerFee    float64 `json:"takerFee"`
	PriceFilter float64 `json:"priceFilter"`
	LotSize     float64 `json:"lotSize"`
}

type OrderBook struct {
	Bids [][]float64 `json:"bids"`
	Asks [][]float64 `json:"asks"`
}

/********** Private API Structure**********/
type ContractBalances []struct {
	Symbol            string  `json:"symbol"`
	MarginBalance     float64 `json:"margin_balance"`
	MarginPosition    float64 `json:"margin_position"`
	MarginFrozen      float64 `json:"margin_frozen"`
	MarginAvailable   float64 `json:"margin_available"`
	ProfitReal        float64 `json:"profit_real"`
	ProfitUnreal      float64 `json:"profit_unreal"`
	WithdrawAvailable float64 `json:"withdraw_available"`
	RiskRate          float64 `json:"risk_rate"`
	LiquidationPrice  float64 `json:"liquidation_price"`
	AdjustFactor      float64 `json:"adjust_factor"`
	MarginStatic      float64 `json:"margin_static"`
}

type AccountsReturn []struct {
	ID      int64  `json:"id"`
	Type    string `json:"type"`
	State   string `json:"state"`
	SubType string `json:"subtype"`
}

type AccountBalances struct {
	ID    int    `json:"id"`
	Type  string `json:"type"`
	State string `json:"state"`
	List  []struct {
		Currency string `json:"currency"`
		Type     string `json:"type"`
		Balance  string `json:"balance"`
		Address  string `json:"address"`
	} `json:"list"`
}

type ContractOrderStatus []struct {
	Symbol         string  `json:"symbol"`
	ContractType   string  `json:"contract_type"`
	ContractCode   string  `json:"contract_code"`
	Volume         float64 `json:"volume"`
	Price          float64 `json:"price"`
	OrderPriceType string  `json:"order_price_type"`
	Direction      string  `json:"direction"`
	Offset         string  `json:"offset"`
	LeverRate      float64 `json:"lever_rate"`
	OrderID        int     `json:"order_id"`
	ClientOrderID  int     `json:"client_order_id"`
	OrderSource    string  `json:"order_source"`
	CreatedAt      int64   `json:"created_at"`
	TradeVolume    float64 `json:"trade_volume"`
	TradeTurnover  float64 `json:"trade_turnover"`
	Fee            float64 `json:"fee"`
	TradeAvgPrice  float64 `json:"trade_avg_price"`
	MarginFrozen   float64 `json:"margin_frozen"`
	Profit         float64 `json:"profit "`
	Status         int     `json:"status"`
}

type Transfer struct {
	Status  string `json:"status"`
	Data    int64  `json:"data"`
	ErrCode string `json:"err-code"`
	ErrMsg  string `json:"err-msg"`
}

type GetAddr []struct {
	Currency   string `json:"currency"`
	Address    string `json:"address"`
	AddressTag string `json:"addressTag"`
	Chain      string `json:"chain"`
}

type WithdrawResponse struct {
	Data int `json:"data"`
}

type PlaceOrder struct {
	Status        string `json:"status"`
	OrderID       int64  `json:"order_id"`
	ClientOrderID int    `json:"client_order_id"`
	Ts            int64  `json:"ts"`
}
