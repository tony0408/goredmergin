package huobidm

// Copyright (c) 2015-2019 Bitontop Technologies Inc.
// Distributed under the MIT software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"math"
	"net/http"
	"net/url"
	"sort"
	"strconv"
	"strings"
	"time"

	"github.com/bitontop/gored/coin"
	"github.com/bitontop/gored/exchange"
	"github.com/bitontop/gored/pair"
	mExchange "gitlab.com/tony0408/goredmergin/exchange"
)

/*The Base Endpoint URL*/
const (
	API_URL     = "https://api.hbdm.com"
	API_URL_PRO = "https://api.huobi.pro"
)

/*API Base Knowledge
Path: API function. Usually after the base endpoint URL
Method:
	Get - Call a URL, API return a response
	Post - Call a URL & send a request, API return a response
Public API:
	It doesn't need authorization/signature , can be called by browser to get response.
	using exchange.HttpGetRequest/exchange.HttpPostRequest
Private API:
	Authorization/Signature is requried. The signature request should look at Exchange API Document.
	using ApiKeyGet/ApiKeyPost
Response:
	Response is a json structure.
	Copy the json to https://transform.now.sh/json-to-go/ convert to go Struct.
	Add the go Struct to model.go

ex. Get /api/v1/depth
Get - Method
/api/v1/depth - Path*/

/*************** Public API ***************/
/*Get Coins Information (If API provide)
Step 1: Change Instance Name    (e *<exchange Instance Name>)
Step 2: Add Model of API Response
Step 3: Modify API Path(strRequestPath)*/
func (e *Huobidm) GetCoinsData() error {
	jsonResponse := &JsonResponse{}
	contractsData := ContractsData{}

	strRequestPath := "/api/v1/contract_contract_info"
	strUrl := API_URL + strRequestPath

	jsonCurrencyReturn := exchange.HttpGetRequest(strUrl, nil)
	if err := json.Unmarshal([]byte(jsonCurrencyReturn), &jsonResponse); err != nil {
		return fmt.Errorf("%s Get Coins Json Unmarshal Err: %v %v", e.GetName(), err, jsonCurrencyReturn)
	} else if jsonResponse.Status != "ok" {
		return fmt.Errorf("%s Get Coins Failed: %v", e.GetName(), jsonResponse)
	}
	if err := json.Unmarshal(jsonResponse.Data, &contractsData); err != nil {
		return fmt.Errorf("%s Get Coins Result Unmarshal Err: %v %s", e.GetName(), err, jsonResponse.Data)
	}

	for _, data := range contractsData {
		c := &coin.Coin{}
		switch e.Source {
		case exchange.EXCHANGE_API:
			c = coin.GetCoin(GetContractName(data.ContractType) + data.Symbol)
			if c == nil {
				c = &coin.Coin{
					Code:     GetContractName(data.ContractType) + data.Symbol,
					Name:     data.Symbol,
					Explorer: data.ContractType,
				}
				coin.AddCoin(c)
			}
		case exchange.JSON_FILE:
			c = e.GetCoinBySymbol(GetContractName(data.ContractType) + data.Symbol)
		}

		if c != nil {
			coinConstraint := &exchange.CoinConstraint{
				CoinID:       c.ID,
				Coin:         c,
				ExSymbol:     strings.ToLower(data.Symbol),
				ChainType:    exchange.MAINNET,
				TxFee:        DEFAULT_TXFEE,
				Withdraw:     DEFAULT_WITHDRAW,
				Deposit:      DEFAULT_DEPOSIT,
				Confirmation: DEFAULT_CONFIRMATION,
				Listed:       DEFAULT_LISTED,
			}

			e.SetCoinConstraint(coinConstraint)
		}
	}

	// add USD
	c := &coin.Coin{}
	if e.Source == exchange.EXCHANGE_API {
		c = &coin.Coin{
			ID:   311,
			Code: "USD",
			Name: "US Dollar",
		}
		coin.AddCoin(c)
	}
	coinConstraint := &exchange.CoinConstraint{
		CoinID:       c.ID,
		Coin:         c,
		ExSymbol:     "usd",
		ChainType:    exchange.MAINNET,
		TxFee:        DEFAULT_TXFEE,
		Withdraw:     DEFAULT_WITHDRAW,
		Deposit:      DEFAULT_DEPOSIT,
		Confirmation: DEFAULT_CONFIRMATION,
		Listed:       DEFAULT_LISTED,
	}
	e.SetCoinConstraint(coinConstraint)

	return nil
}

/* GetPairsData - Get Pairs Information (If API provide)
Step 1: Change Instance Name    (e *<exchange Instance Name>)
Step 2: Add Model of API Response
Step 3: Modify API Path(strRequestUrl)*/

func (e *Huobidm) GetPairsData() error {
	jsonResponse := &JsonResponse{}
	contractsData := ContractsData{}

	strRequestPath := "/api/v1/contract_contract_info"
	strUrl := API_URL + strRequestPath

	jsonSymbolsReturn := exchange.HttpGetRequest(strUrl, nil)
	if err := json.Unmarshal([]byte(jsonSymbolsReturn), &jsonResponse); err != nil {
		return fmt.Errorf("%s Get Pairs Json Unmarshal Err: %v %v", e.GetName(), err, jsonSymbolsReturn)
	} else if jsonResponse.Status != "ok" {
		return fmt.Errorf("%s Get Pairs Failed: %v", e.GetName(), jsonResponse)
	}
	if err := json.Unmarshal(jsonResponse.Data, &contractsData); err != nil {
		return fmt.Errorf("%s Get Pairs Result Unmarshal Err: %v %s", e.GetName(), err, jsonResponse.Data)
	}

	for _, data := range contractsData {
		if data.ContractStatus == 1 {
			p := &pair.Pair{}
			switch e.Source {
			case exchange.EXCHANGE_API:
				base := coin.GetCoin("USD")
				target := coin.GetCoin(GetContractName(data.ContractType) + data.Symbol)
				if base != nil && target != nil {
					p = pair.GetPair(base, target)
				}
			case exchange.JSON_FILE:
				p = e.GetPairBySymbol(data.Symbol + "_" + data.ContractType)
			}
			if p != nil {
				pairConstraint := &exchange.PairConstraint{
					PairID:      p.ID,
					Pair:        p,
					ExSymbol:    data.Symbol + "_" + data.ContractType,
					ExID:        strings.ToLower(data.Symbol),
					MakerFee:    DEFAULT_MAKER_FEE,
					TakerFee:    DEFAULT_TAKER_FEE,
					LotSize:     data.ContractSize,
					PriceFilter: data.PriceTick,
					Listed:      DEFAULT_LISTED,
				}
				e.SetPairConstraint(pairConstraint)
			}
		}
	}
	return nil
}

/*Get Pair Market Depth
Step 1: Change Instance Name    (e *<exchange Instance Name>)
Step 2: Add Model of API Response
Step 3: Get Exchange Pair Code ex. symbol := e.GetSymbolByPair(p)
Step 4: Modify API Path(strRequestUrl)
Step 5: Add Params - Depend on API request
Step 6: Convert the response to Standard Maker struct*/
func (e *Huobidm) OrderBook(p *pair.Pair) (*exchange.Maker, error) {
	jsonResponse := &JsonResponse2{}
	orderBook := OrderBook{}

	mapParams := make(map[string]string)
	mapParams["symbol"] = p.Target.Name + "_" + GetContractName(p.Target.Explorer)
	mapParams["type"] = "step0"

	strRequestPath := "/market/depth"
	strUrl := API_URL + strRequestPath

	maker := &exchange.Maker{
		WorkerIP:        exchange.GetExternalIP(),
		Source:          exchange.EXCHANGE_API,
		BeforeTimestamp: float64(time.Now().UnixNano() / 1e6),
	}

	jsonOrderbook := exchange.HttpGetRequest(strUrl, mapParams)
	if err := json.Unmarshal([]byte(jsonOrderbook), &jsonResponse); err != nil {
		return nil, fmt.Errorf("%s Get Orderbook Json Unmarshal Err: %v %v", e.GetName(), err, jsonOrderbook)
	} else if jsonResponse.Status != "ok" || jsonResponse.Tick == nil {
		return nil, fmt.Errorf("%s Get Orderbook Failed: %v", e.GetName(), jsonResponse)
	}
	if err := json.Unmarshal(jsonResponse.Tick, &orderBook); err != nil {
		return nil, fmt.Errorf("%s Get Orderbook Result Unmarshal Err: %v %s", e.GetName(), err, jsonResponse.Tick)
	}

	maker.AfterTimestamp = float64(time.Now().UnixNano() / 1e6)

	var err error
	for _, bid := range orderBook.Bids {
		buydata := exchange.Order{}
		buydata.Quantity = bid[1]
		buydata.Rate = bid[0]
		maker.Bids = append(maker.Bids, buydata)
	}

	for _, ask := range orderBook.Asks {
		selldata := exchange.Order{}
		selldata.Quantity = ask[1]
		selldata.Rate = ask[0]
		maker.Asks = append(maker.Asks, selldata)
	}

	return maker, err
}

/*************** Private API ***************/
func (e *Huobidm) GetAccounts(p *pair.Pair) string {
	if e.API_KEY == "" || e.API_SECRET == "" {
		log.Printf("%s API Key or Secret Key are nil", e.GetName())
		return ""
	}

	jsonResponse := &JsonResponse{}
	accountsReturn := AccountsReturn{}

	strRequest := "/v1/account/accounts"

	jsonAccountsReturn := e.ApiOldRequest("GET", make(map[string]string), strRequest)
	if err := json.Unmarshal([]byte(jsonAccountsReturn), &jsonResponse); err != nil {
		log.Printf("%s Get AccountID Json Unmarshal Err: %v %v", e.GetName(), err, jsonAccountsReturn)
		return ""
	} else if jsonResponse.Status != "ok" {
		log.Printf("%s Get AccountID Failed: %v", e.GetName(), jsonResponse)
		return ""
	}
	if err := json.Unmarshal(jsonResponse.Data, &accountsReturn); err != nil {
		log.Printf("%s Get AccountID Data Unmarshal Err: %v %s", e.GetName(), err, jsonResponse.Data)
		return ""
	}

	accountID := ""
	for _, account := range accountsReturn {
		if p == nil && account.Type == "spot" {
			accountID = fmt.Sprintf("%d", account.ID)
			break
		} else if account.SubType == e.GetSymbolByPair(p) {
			accountID = fmt.Sprintf("%d", account.ID)
			break
		}
	}

	return accountID
}

func (e *Huobidm) UpdateAllBalances() {
	if e.API_KEY == "" || e.API_SECRET == "" {
		log.Printf("%s API Key or Secret Key are nil.", e.GetName())
		return
	}

	if e.Account_ID == "" {
		e.Account_ID = e.GetAccounts(nil)
		if e.Account_ID == "" {
			return
		}
	}

	jsonResponse := &JsonResponse{}
	accountBalance := AccountBalances{}
	strRequest := fmt.Sprintf("/v1/account/accounts/%s/balance", e.Account_ID)

	jsonBalanceReturn := e.ApiOldRequest("GET", make(map[string]string), strRequest)
	if err := json.Unmarshal([]byte(jsonBalanceReturn), &jsonResponse); err != nil {
		log.Printf("%s UpdateAllBalances Json Unmarshal Err: %v %v", e.GetName(), err, jsonBalanceReturn)
		return
	} else if jsonResponse.Status != "ok" {
		log.Printf("%s UpdateAllBalances Failed: %v", e.GetName(), jsonResponse)
		return
	}
	if err := json.Unmarshal(jsonResponse.Data, &accountBalance); err != nil {
		log.Printf("%s UpdateAllBalances Data Unmarshal Err: %v %s", e.GetName(), err, jsonResponse.Data)
		return
	}

	for _, v := range accountBalance.List {
		if v.Type == "trade" {
			freeamount, err := strconv.ParseFloat(v.Balance, 64)
			if err == nil {
				c := e.GetCoinBySymbol(strings.ToLower(v.Currency))
				if c != nil {
					balanceMap.Set(e.GetSymbolByCoin(c) /* c.Code */, freeamount)
				}
			} else {
				log.Printf("%s %s Get Balance Err: %s\n", e.GetName(), v.Currency, err)
			}
		}
	}
}

/* Withdraw(coin *coin.Coin, quantity float64, addr, tag string) */
func (e *Huobidm) Withdraw(coin *coin.Coin, quantity float64, addr, tag string) bool {
	if e.API_KEY == "" || e.API_SECRET == "" {
		log.Printf("%s API Key or Secret Key are nil.", e.GetName())
		return false
	}

	withdraw := WithdrawResponse{}
	strRequestPath := "/v1/dw/withdraw/api/create"

	mapParams := make(map[string]string)
	mapParams["currency"] = e.GetSymbolByCoin(coin)
	mapParams["amount"] = fmt.Sprintf("%v", quantity)
	mapParams["address"] = addr
	mapParams["fee"] = tag // The fee to pay with this withdraw(txfee?)

	jsonSubmitWithdraw := e.ProKeyRequest("POST", strRequestPath, mapParams)
	if err := json.Unmarshal([]byte(jsonSubmitWithdraw), &withdraw); err != nil {
		log.Printf("%s Withdraw Json Unmarshal Err: %v %v", e.GetName(), err, jsonSubmitWithdraw)
		return false
	} else if withdraw.Data == 0 {
		log.Printf("%s Withdraw Failed: %v", e.GetName(), jsonSubmitWithdraw)
		return false
	}

	log.Printf("Withdraw transfer ID: %v", withdraw.Data)

	return true
}

func (e *Huobidm) LimitSell(pair *pair.Pair, quantity, rate float64) (*exchange.Order, error) {
	if e.API_KEY == "" || e.API_SECRET == "" {
		return nil, fmt.Errorf("%s API Key or Secret Key are nil.", e.GetName())
	}

	jsonResponse := &JsonResponse{}
	placeOrder := PlaceOrder{}
	strRequestPath := "/API Path"

	mapParams := make(map[string]string)
	mapParams["symbol"] = e.GetSymbolByPair(pair)
	mapParams["side"] = "SELL"
	mapParams["type"] = "LIMIT"
	mapParams["price"] = strconv.FormatFloat(rate, 'f', -1, 64)
	mapParams["quantity"] = strconv.FormatFloat(quantity, 'f', -1, 64)

	jsonPlaceReturn := e.ApiKeyRequest("POST", strRequestPath, mapParams)
	if err := json.Unmarshal([]byte(jsonPlaceReturn), &jsonResponse); err != nil {
		return nil, fmt.Errorf("%s LimitSell Json Unmarshal Err: %v %v", e.GetName(), err, jsonPlaceReturn)
	} else if jsonResponse.Status != "ok" {
		return nil, fmt.Errorf("%s LimitSell Failed: %v", e.GetName(), jsonPlaceReturn)
	}
	if err := json.Unmarshal(jsonResponse.Data, &placeOrder); err != nil {
		return nil, fmt.Errorf("%s LimitSell Result Unmarshal Err: %v %s", e.GetName(), err, jsonResponse.Data)
	}

	order := &exchange.Order{
		Pair:         pair,
		OrderID:      "", //placeOrder.OrderID,
		Rate:         rate,
		Quantity:     quantity,
		Side:         "Sell",
		Status:       exchange.New,
		JsonResponse: jsonPlaceReturn,
	}
	return order, nil
}

// not real limitBuy
func (e *Huobidm) LimitBuy(pair *pair.Pair, quantity, rate float64) (*exchange.Order, error) {
	if e.API_KEY == "" || e.API_SECRET == "" {
		return nil, fmt.Errorf("%s API Key or Secret Key are nil.", e.GetName())
	}

	jsonResponse := &JsonResponse{}
	placeOrder := PlaceOrder{}
	strRequestPath := "/api/v1/contract_order"

	mapParams := make(map[string]string)
	// mandatory
	mapParams["direction"] = "buy"
	mapParams["price"] = strconv.FormatFloat(rate, 'f', -1, 64)
	mapParams["volume"] = strconv.FormatFloat(quantity, 'f', -1, 64)
	mapParams["lever_rate"] = "10"
	mapParams["offset"] = "open"
	mapParams["order_price_type"] = "limit"

	// optional
	mapParams["symbol"] = e.getApiSymbolByPair(pair)
	mapParams["contract_type"] = e.getApiContractTypeByPair(pair)

	jsonPlaceReturn := e.ApiKeyRequest("POST", strRequestPath, mapParams)
	// log.Printf("==jsonPlaceReturn: %v", jsonPlaceReturn)
	if err := json.Unmarshal([]byte(jsonPlaceReturn), &jsonResponse); err != nil {
		return nil, fmt.Errorf("%s LimitBuy Json Unmarshal Err: %v %v", e.GetName(), err, jsonPlaceReturn)
	} else if jsonResponse.Status != "ok" {
		return nil, fmt.Errorf("%s LimitBuy Failed: %v", e.GetName(), jsonPlaceReturn)
	}
	if err := json.Unmarshal(jsonResponse.Data, &placeOrder); err != nil {
		return nil, fmt.Errorf("%s LimitBuy Result Unmarshal Err: %v %s", e.GetName(), err, jsonResponse.Data)
	}

	order := &exchange.Order{
		Pair:         pair,
		OrderID:      "", //placeOrder.OrderID,
		Rate:         rate,
		Quantity:     quantity,
		Side:         "Buy",
		Status:       exchange.New,
		JsonResponse: jsonPlaceReturn,
	}
	return order, nil
}

func (e *Huobidm) OrderStatus(order *exchange.Order) error {
	if e.API_KEY == "" || e.API_SECRET == "" {
		return fmt.Errorf("%s API Key or Secret Key are nil.", e.GetName())
	}

	jsonResponse := &JsonResponse{}
	orderStatus := ContractOrderStatus{}
	strRequestPath := "/api/v1/contract_order_info" //"/api/v1/contract_order_detail" // //

	mapParams := make(map[string]string)
	mapParams["symbol"] = e.getApiSymbolByPair(order.Pair)
	mapParams["orderId"] = order.OrderID

	jsonOrderStatus := e.ApiKeyRequest("POST", strRequestPath, mapParams)
	// log.Printf("jsonOrderStatus: %v", jsonOrderStatus)
	if err := json.Unmarshal([]byte(jsonOrderStatus), &jsonResponse); err != nil {
		return fmt.Errorf("%s OrderStatus Json Unmarshal Err: %v %v", e.GetName(), err, jsonOrderStatus)
	} else if jsonResponse.Status != "ok" {
		return fmt.Errorf("%s OrderStatus Failed: %v", e.GetName(), jsonOrderStatus)
	}
	if err := json.Unmarshal(jsonResponse.Data, &orderStatus); err != nil {
		return fmt.Errorf("%s OrderStatus Result Unmarshal Err: %v %s", e.GetName(), err, jsonResponse.Data)
	}

	if len(orderStatus) == 0 {
		return fmt.Errorf("%s OrderStatus Failed: %v", e.GetName(), jsonOrderStatus)
	}
	if orderStatus[0].Status == 1 || orderStatus[0].Status == 2 || orderStatus[0].Status == 3 {
		order.Status = exchange.New
	} else if orderStatus[0].Status == 4 {
		order.Status = exchange.Partial
	} else if orderStatus[0].Status == 5 || orderStatus[0].Status == 7 {
		order.Status = exchange.Cancelled
	} else if orderStatus[0].Status == 11 {
		order.Status = exchange.Canceling
	} else if orderStatus[0].Status == 6 {
		order.Status = exchange.Filled
	} else {
		order.Status = exchange.Other
	}

	// order.DealRate, _ = strconv.ParseFloat(orderStatus.AveragePrice, 64)
	// order.DealQuantity, _ = strconv.ParseFloat(orderStatus.ExecutedQty, 64)

	return nil
}

func (e *Huobidm) ListOrders() ([]*exchange.Order, error) {
	return nil, nil
}

func (e *Huobidm) CancelOrder(order *exchange.Order) error {
	if e.API_KEY == "" || e.API_SECRET == "" {
		return fmt.Errorf("%s API Key or Secret Key are nil.", e.GetName())
	}

	jsonResponse := &JsonResponse{}
	cancelOrder := PlaceOrder{}
	strRequestPath := "/API Path"

	mapParams := make(map[string]string)
	mapParams["symbol"] = e.GetSymbolByPair(order.Pair)
	mapParams["orderId"] = order.OrderID

	jsonCancelOrder := e.ApiKeyRequest("DELETE", strRequestPath, mapParams)
	if err := json.Unmarshal([]byte(jsonCancelOrder), &jsonResponse); err != nil {
		return fmt.Errorf("%s CancelOrder Json Unmarshal Err: %v %v", e.GetName(), err, jsonCancelOrder)
	} else if jsonResponse.Status != "ok" {
		return fmt.Errorf("%s CancelOrder Failed: %v", e.GetName(), jsonCancelOrder)
	}
	if err := json.Unmarshal(jsonResponse.Data, &cancelOrder); err != nil {
		return fmt.Errorf("%s CancelOrder Result Unmarshal Err: %v %s", e.GetName(), err, jsonResponse.Data)
	}

	order.Status = exchange.Canceling
	order.CancelStatus = jsonCancelOrder

	return nil
}

func (e *Huobidm) CancelAllOrder() error {
	return nil
}

func (e *Huobidm) MarginAction(data *exchange.Margin) error {
	return nil
}

func (e *Huobidm) ContractAction(data *mExchange.Contract) error {
	if e.API_KEY == "" || e.API_SECRET == "" {
		return fmt.Errorf("%s API Key or Secret Key are nil.", e.GetName())
	}

	switch data.Action {
	case mExchange.CONTRACT_MARKET_BUY, mExchange.CONTRACT_MARKET_SELL, mExchange.CONTRACT_LIMIT_BUY, mExchange.CONTRACT_LIMIT_SELL:
		return e.contractPlaceOrder(data)
	case mExchange.CONTRACT_TRANSFER:
		return e.transfer(data)
	case mExchange.GET_ADDR:
		return e.getAddr(data)
	case mExchange.CONTRACT_ORDER_STATUS:
		return e.contractOrderStatus(data)
	case mExchange.CONTRACT_BALANCE:
		return e.contractBalances(data)
	}

	return nil
}

func (e *Huobidm) contractBalances(data *mExchange.Contract) error {
	if data.Currency == nil {
		return fmt.Errorf("%s Get ContractBalances Coin is null", e.GetName())
	}

	jsonResponse := &JsonResponse{}
	accountBalance := ContractBalances{}

	strRequestPath := "/api/v1/contract_account_info"

	mapParams := make(map[string]string)
	mapParams["symbol"] = e.GetSymbolByCoin(data.Currency) // e.g. "BTC"

	jsonBalanceReturn := e.ApiKeyRequest("POST", strRequestPath, mapParams)
	// log.Printf("===========jsonBalanceReturn: %+v", jsonBalanceReturn) // =================
	if err := json.Unmarshal([]byte(jsonBalanceReturn), &jsonResponse); err != nil {
		return fmt.Errorf("%s Get ContractBalances Json Unmarshal Err: %v %s", e.GetName(), err, jsonBalanceReturn)
	} else if jsonResponse.Status != "ok" {
		return fmt.Errorf("%s Get ContractBalances Failed: %v", e.GetName(), jsonBalanceReturn)
	}
	if err := json.Unmarshal(jsonResponse.Data, &accountBalance); err != nil {
		return fmt.Errorf("%s Get ContractBalances Result Unmarshal Err: %v %s", e.GetName(), err, jsonResponse.Data)
	}

	// Write into return struct
	for _, balance := range accountBalance {
		if balance.Symbol == strings.ToUpper(mapParams["symbol"]) {
			data.ContractBalanceReturn = &mExchange.ContractBalance{
				MarginBalance:     balance.MarginBalance,
				MarginAvailable:   balance.MarginAvailable,
				WithdrawAvailable: balance.WithdrawAvailable,
			}

			return nil
		}
	}

	return fmt.Errorf("%s Get ContractBalances Failed, didn't get %s balance, %s", e.GetName(), strings.ToUpper(mapParams["symbol"]), jsonBalanceReturn)
}

// offset: "open", "close"
func (e *Huobidm) contractPlaceOrder(data *mExchange.Contract) error { //*exchange.Order
	jsonResponse := &JsonResponse{}
	placeOrder := PlaceOrder{}
	strRequestPath := "/api/v1/contract_order"

	priceFilter := int(math.Round(math.Log10(e.GetPriceFilter(data.Pair)) * -1))
	lotSize := e.GetLotSize(data.Pair)
	if lotSize == 0 {
		lotSize = 100
	}

	mapParams := make(map[string]string)
	// mandatory
	switch data.Action {
	case exchange.CONTRACT_LIMIT_BUY, exchange.CONTRACT_MARKET_BUY:
		mapParams["direction"] = "buy"
	case exchange.CONTRACT_LIMIT_SELL, exchange.CONTRACT_MARKET_SELL:
		mapParams["direction"] = "sell"
	}
	mapParams["volume"] = fmt.Sprintf("%.0f", data.Quantity/lotSize)
	mapParams["lever_rate"] = strconv.FormatFloat(data.Leverage, 'f', -1, 64)
	mapParams["offset"] = string(data.Offset)
	mapParams["order_price_type"] = string(data.OrderPriceType)
	if data.Rate != 0 {
		mapParams["price"] = strconv.FormatFloat(data.Rate, 'f', priceFilter, 64)
	}

	// optional
	mapParams["symbol"] = e.getApiSymbolByPair(data.Pair)
	mapParams["contract_type"] = e.getApiContractTypeByPair(data.Pair)

	jsonPlaceReturn := e.ApiKeyRequest("POST", strRequestPath, mapParams)
	if err := json.Unmarshal([]byte(jsonPlaceReturn), &jsonResponse); err != nil {
		return fmt.Errorf("%s contractPlaceOrder Json Unmarshal Err: %v %s", e.GetName(), err, jsonPlaceReturn)
	} else if jsonResponse.Status != "ok" {
		return fmt.Errorf("%s contractPlaceOrder Failed: %s", e.GetName(), jsonPlaceReturn)
	}
	if err := json.Unmarshal(jsonResponse.Data, &placeOrder); err != nil {
		return fmt.Errorf("%s contractPlaceOrder Result Unmarshal Err: %v %s", e.GetName(), err, jsonResponse.Data)
	}

	// Write into return struct
	data.Order = &exchange.Order{
		OrderID:   fmt.Sprintf("%d", placeOrder.OrderID),
		Pair:      data.Pair,
		Rate:      data.Rate,
		Quantity:  data.Quantity,
		Side:      mapParams["direction"],
		OrderType: mapParams["order_price_type"],
		Leverage:  data.Leverage,
		Status:    exchange.New,
	}

	return nil
}

func (e *Huobidm) contractOrderStatus(data *mExchange.Contract) error {
	jsonResponse := &JsonResponse{}
	orderStatus := ContractOrderStatus{}
	strRequestPath := "/api/v1/contract_order_info" //"/api/v1/contract_order_detail" // //

	mapParams := make(map[string]string)
	mapParams["symbol"] = e.getApiSymbolByPair(data.Order.Pair)
	// optional
	if data.Order.OrderID != "" {
		mapParams["order_id"] = data.Order.OrderID
	}
	// log.Printf(`e.getApiSymbolByPair(data.Order.Pair): %v`, e.getApiSymbolByPair(data.Order.Pair)) // =========

	jsonOrderStatus := e.ApiKeyRequest("POST", strRequestPath, mapParams)
	// log.Printf("jsonOrderStatus: %v", jsonOrderStatus)
	if err := json.Unmarshal([]byte(jsonOrderStatus), &jsonResponse); err != nil {
		return fmt.Errorf("%s OrderStatus Json Unmarshal Err: %v %s", e.GetName(), err, jsonOrderStatus)
	} else if jsonResponse.Status != "ok" {
		return fmt.Errorf("%s OrderStatus Failed: %v", e.GetName(), jsonOrderStatus)
	}
	if err := json.Unmarshal(jsonResponse.Data, &orderStatus); err != nil {
		return fmt.Errorf("%s OrderStatus Result Unmarshal Err: %v %s", e.GetName(), err, jsonResponse.Data)
	}

	if len(orderStatus) == 0 {
		return fmt.Errorf("%s OrderStatus Failed: %s", e.GetName(), jsonOrderStatus)
	}

	for _, order := range orderStatus {
		if fmt.Sprintf("%d", order.OrderID) != data.Order.OrderID {
			continue
		}

		if order.Status == 1 || order.Status == 2 || order.Status == 3 {
			data.Order.Status = exchange.New
		} else if order.Status == 4 {
			data.Order.Status = exchange.Partial
		} else if order.Status == 5 || order.Status == 7 {
			data.Order.Status = exchange.Canceled
		} else if order.Status == 11 {
			data.Order.Status = exchange.Canceling
		} else if order.Status == 6 {
			data.Order.Status = exchange.Filled
		} else {
			data.Order.Status = exchange.Other
		}
		data.Order.DealQuantity = order.TradeTurnover
		data.Order.DealRate = order.TradeAvgPrice
		return nil
	}

	return fmt.Errorf("%s Could not find Order: %s", e.GetName(), data.Order.OrderID)
}

func (e *Huobidm) transfer(data *exchange.Contract) error {
	trans := Transfer{}
	strRequestPath := "/v1/futures/transfer"

	mapParams := make(map[string]string)
	mapParams["currency"] = e.GetSymbolByCoin(data.Currency)             // data.Currency.Code
	mapParams["amount"] = strconv.FormatFloat(data.Quantity, 'f', 8, 64) //****************
	mapParams["type"] = string(data.Direction)                           // Future to Spot: "futures-to-pro"; Spot to Future: "pro-to-futures"

	jsonTransReturn := e.ProKeyRequest("POST", strRequestPath, mapParams)
	if err := json.Unmarshal([]byte(jsonTransReturn), &trans); err != nil {
		return fmt.Errorf("%s Transfer margin Json Unmarshal Err: %v %s", e.GetName(), err, jsonTransReturn)
	} else if trans.Status != "ok" {
		return fmt.Errorf("%s Transfer margin Failed: %s", e.GetName(), jsonTransReturn)
	}

	data.TransferID = trans.Data

	return nil
}

func (e *Huobidm) getAddr(data *exchange.Contract) error {
	jsonResponse := &JsonResponse3{}
	getAddr := GetAddr{}
	strRequestPath := "/v2/account/deposit/address"

	mapParams := make(map[string]string)
	mapParams["currency"] = e.GetSymbolByCoin(data.Currency)

	jsonAddrReturn := e.ProKeyRequest("GET", strRequestPath, mapParams)
	if err := json.Unmarshal([]byte(jsonAddrReturn), &jsonResponse); err != nil {
		return fmt.Errorf("%s GetAddr Json Unmarshal Err: %v %s", e.GetName(), err, jsonAddrReturn)
	} else if jsonResponse.Code != 200 {
		return fmt.Errorf("%s GetAddr Failed: %s", e.GetName(), jsonAddrReturn)
	}
	if err := json.Unmarshal(jsonResponse.Data, &getAddr); err != nil {
		return fmt.Errorf("%s GetAddr Result Unmarshal Err: %v %s", e.GetName(), err, jsonResponse.Data)
	}

	for _, account := range getAddr {
		if account.Currency == e.GetSymbolByCoin(data.Currency) {
			data.Addr = account.Address
		}
	}

	return nil
}

// helper methods
func (e *Huobidm) getApiSymbolByPair(pair *pair.Pair) string {
	symbols := strings.SplitN(e.GetSymbolByPair(pair), "_", 2)
	return symbols[0]
}

func (e *Huobidm) getApiContractTypeByPair(pair *pair.Pair) string {
	symbols := strings.SplitN(e.GetSymbolByPair(pair), "_", 2)
	if len(symbols) >= 2 {
		return symbols[1]
	} else {
		return ""
	}
}

/*************** Signature Http Request ***************/
/*Method: API Get Request and Signature is required
Step 1: Change Instance Name    (e *<exchange Instance Name>)
Step 2: Create mapParams Depend on API Signature request
Step 3: Add HttpGetRequest below strUrl if API has different requests*/
func (e *Huobidm) ApiKeyGet(strRequestPath string, mapParams map[string]string) string {
	mapParams["signature"] = exchange.ComputeHmac256NoDecode(exchange.Map2UrlQuery(mapParams), e.API_SECRET)

	payload := exchange.Map2UrlQuery(mapParams)
	strUrl := API_URL + strRequestPath + "?" + payload

	request, err := http.NewRequest("GET", strUrl, nil)
	if nil != err {
		return err.Error()
	}
	request.Header.Add("Content-Type", "application/json; charset=utf-8")
	request.Header.Add("X-MBX-APIKEY", e.API_KEY)

	httpClient := &http.Client{}
	response, err := httpClient.Do(request)
	if nil != err {
		return err.Error()
	}
	defer response.Body.Close()

	body, err := ioutil.ReadAll(response.Body)
	if nil != err {
		return err.Error()
	}

	return string(body)
}

/*Method: API Request and Signature is required
Step 1: Change Instance Name    (e *<exchange Instance Name>)
Step 2: Create mapParams Depend on API Signature request*/
func (e *Huobidm) ApiKeyRequest(strMethod string, strRequestPath string, mapParams map[string]string) string {
	timestamp := time.Now().UTC().Format("2006-01-02T15:04:05")
	strUrl := API_URL + strRequestPath

	mapParams["AccessKeyId"] = e.API_KEY
	mapParams["SignatureMethod"] = "HmacSHA256"
	mapParams["SignatureVersion"] = "2"
	mapParams["Timestamp"] = timestamp

	hostName := "api.hbdm.com"
	mapParams["Signature"] = CreateSign(mapParams, strMethod, hostName, strRequestPath, e.API_SECRET)
	// log.Printf("====mapParams: %+v", mapParams) // ===============
	var strRequestUrl string
	strParams := MapSortByKey(mapParams)
	strRequestUrl = strUrl + "?" + strParams

	if strMethod == "POST" {
		return exchange.HttpPostRequest(strRequestUrl, mapParams)
	}

	// 构建Request, 并且按官方要求添加Http Header
	request, err := http.NewRequest("GET", strRequestUrl, nil)
	if nil != err {
		return err.Error()
	}
	request.Header.Add("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.71 Safari/537.36")

	// 发出请求
	httpClient := &http.Client{}
	response, err := httpClient.Do(request)
	if nil != err {
		return err.Error()
	}
	defer response.Body.Close()

	// 解析响应内容
	body, err := ioutil.ReadAll(response.Body)
	if nil != err {
		return err.Error()
	}

	return string(body)
}

func (e *Huobidm) ProKeyRequest(strMethod string, strRequestPath string, mapParams map[string]string) string {
	timestamp := time.Now().UTC().Format("2006-01-02T15:04:05")
	strUrl := API_URL_PRO + strRequestPath

	mapParams["AccessKeyId"] = e.API_KEY
	mapParams["SignatureMethod"] = "HmacSHA256"
	mapParams["SignatureVersion"] = "2"
	mapParams["Timestamp"] = timestamp

	hostName := "api.huobi.pro" //"api.hbdm.com"
	mapParams["Signature"] = CreateSign(mapParams, strMethod, hostName, strRequestPath, e.API_SECRET)
	// log.Printf("====mapParams: %+v", mapParams) // ===============
	var strRequestUrl string
	strParams := MapSortByKey(mapParams)
	strRequestUrl = strUrl + "?" + strParams

	if strMethod == "POST" {
		return exchange.HttpPostRequest(strRequestUrl, mapParams)
	}

	// 构建Request, 并且按官方要求添加Http Header
	request, err := http.NewRequest("GET", strRequestUrl, nil)
	if nil != err {
		return err.Error()
	}
	request.Header.Add("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.71 Safari/537.36")

	// 发出请求
	httpClient := &http.Client{}
	response, err := httpClient.Do(request)
	if nil != err {
		return err.Error()
	}
	defer response.Body.Close()

	// 解析响应内容
	body, err := ioutil.ReadAll(response.Body)
	if nil != err {
		return err.Error()
	}

	return string(body)
}

func CreateSign(mapParams map[string]string, strMethod, strHostUrl, strRequestPath, strSecretKey string) string {
	sortedParams := MapSortByKey(mapParams) //将数据根据ASCII进行排序
	strPayload := strMethod + "\n" + strHostUrl + "\n" + strRequestPath + "\n" + sortedParams

	// log.Printf("strPayLoad: %v", strPayload)
	return exchange.ComputeHmac256Base64(strPayload, strSecretKey)
}

func MapSortByKey(mapValue map[string]string) string {
	keys := make([]string, 0, len(mapValue))
	for key := range mapValue {
		keys = append(keys, key)
	}
	sort.Strings(keys)
	mapParams := ""
	for _, key := range keys {
		mapParams += (key + "=" + url.QueryEscape(mapValue[key]) + "&")
	}
	mapParams = mapParams[:len(mapParams)-1]
	return mapParams
}

// func (e *Huobidm) ApiKeyRequest(strMethod, strRequestPath string, mapParams map[string]string) string {
// 	strUrl := API_URL + strRequestPath

// 	mapParams["signature"] = exchange.ComputeHmac256NoDecode(exchange.Map2UrlQuery(mapParams), e.API_SECRET)
// 	jsonParams := ""
// 	if nil != mapParams {
// 		bytesParams, _ := json.Marshal(mapParams)
// 		jsonParams = string(bytesParams)
// 	}

// 	request, err := http.NewRequest(strMethod, strUrl, bytes.NewBuffer([]byte(jsonParams)))
// 	if nil != err {
// 		return err.Error()
// 	}
// 	request.Header.Add("Content-Type", "application/json; charset=utf-8")
// 	request.Header.Add("X-MBX-APIKEY", e.API_KEY)

// 	httpClient := &http.Client{}
// 	response, err := httpClient.Do(request)
// 	if nil != err {
// 		return err.Error()
// 	}
// 	defer response.Body.Close()

// 	body, err := ioutil.ReadAll(response.Body)
// 	if nil != err {
// 		return err.Error()
// 	}

// 	return string(body)
// }

// for old balance
func (e *Huobidm) ApiOldRequest(strMethod string, mapParams map[string]string, strRequestPath string) string {

	timestamp := time.Now().UTC().Format("2006-01-02T15:04:05")
	strUrl := API_URL_PRO + strRequestPath

	mapParams["AccessKeyId"] = e.API_KEY
	mapParams["SignatureMethod"] = "HmacSHA256"
	mapParams["SignatureVersion"] = "2"
	mapParams["Timestamp"] = timestamp

	hostName := "api.huobi.pro"
	mapParams["Signature"] = CreateSign(mapParams, strMethod, hostName, strRequestPath, e.API_SECRET)
	// log.Printf("====mapParams: %+v", mapParams)
	var strRequestUrl string
	strParams := MapSortByKey(mapParams)
	strRequestUrl = strUrl + "?" + strParams

	if strMethod == "POST" {
		return exchange.HttpPostRequest(strRequestUrl, mapParams)
	}

	// 构建Request, 并且按官方要求添加Http Header
	request, err := http.NewRequest("GET", strRequestUrl, nil)
	if nil != err {
		return err.Error()
	}
	request.Header.Add("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.71 Safari/537.36")

	// 发出请求
	httpClient := &http.Client{}
	response, err := httpClient.Do(request)
	if nil != err {
		return err.Error()
	}
	defer response.Body.Close()

	// 解析响应内容
	body, err := ioutil.ReadAll(response.Body)
	if nil != err {
		return err.Error()
	}

	return string(body)
}

func GetContractName(code string) string {
	if code == "this_week" {
		return "CW"
	}
	if code == "next_week" {
		return "NW"
	}
	if code == "quarter" {
		return "CQ"
	}
	return "CONTRACT DNE"
}
