package bitmex

// Copyright (c) 2015-2019 Bitontop Technologies Inc.
// Distributed under the MIT software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"math"
	"net/http"
	"strconv"
	"strings"
	"time"

	"gitlab.com/tony0408/goredmergin/coin"
	"gitlab.com/tony0408/goredmergin/exchange"
	"gitlab.com/tony0408/goredmergin/pair"
)

/*The Base Endpoint URL*/
const (
	API_URL = "https://www.bitmex.com"
)

/*API Base Knowledge
Path: API function. Usually after the base endpoint URL
Method:
	Get - Call a URL, API return a response
	Post - Call a URL & send a request, API return a response
Public API:
	It doesn't need authorization/signature , can be called by browser to get response.
	using exchange.HttpGetRequest/exchange.HttpPostRequest
Private API:
	Authorization/Signature is requried. The signature request should look at Exchange API Document.
	using ApiKeyGet/ApiKeyPost
Response:
	Response is a json structure.
	Copy the json to https://transform.now.sh/json-to-go/ convert to go Struct.
	Add the go Struct to model.go

ex. Get /api/v1/depth
Get - Method
/api/v1/depth - Path*/

/*************** Public API ***************/
/*Get Coins Information (If API provide)
Step 1: Change Instance Name    (e *<exchange Instance Name>)
Step 2: Add Model of API Response
Step 3: Modify API Path(strRequestUrl)*/
func (e *Bitmex) GetCoinsData() error {
	coinsData := PairsData{}

	strRequestUrl := "/api/v1/instrument/active"
	strUrl := API_URL + strRequestUrl

	jsonCurrencyReturn := exchange.HttpGetRequest(strUrl, nil)
	if err := json.Unmarshal([]byte(jsonCurrencyReturn), &coinsData); err != nil {
		return fmt.Errorf("%s Get Coins Json Unmarshal Err: %v %v", e.GetName(), err, jsonCurrencyReturn)
	}

	for _, data := range coinsData {
		base := &coin.Coin{}
		target := &coin.Coin{}
		switch e.Source {
		case exchange.EXCHANGE_API:
			base = coin.GetCoin(data.QuoteCurrency)
			if base == nil {
				base = &coin.Coin{}
				base.Code = data.QuoteCurrency
				coin.AddCoin(base)
			}

			target = coin.GetCoin(data.RootSymbol)
			if target == nil {
				target = &coin.Coin{}
				target.Code = data.RootSymbol
				coin.AddCoin(target)
			}
		case exchange.JSON_FILE:
			base = e.GetCoinBySymbol(data.QuoteCurrency)
			target = e.GetCoinBySymbol(data.RootSymbol)
		}

		if base != nil {
			coinConstraint := &exchange.CoinConstraint{
				CoinID:       base.ID,
				Coin:         base,
				ExSymbol:     data.QuoteCurrency,
				ChainType:    exchange.MAINNET,
				TxFee:        DEFAULT_TXFEE,
				Withdraw:     DEFAULT_WITHDRAW,
				Deposit:      DEFAULT_DEPOSIT,
				Confirmation: DEFAULT_CONFIRMATION,
				Listed:       true,
			}

			e.SetCoinConstraint(coinConstraint)
		}

		if target != nil {
			coinConstraint := &exchange.CoinConstraint{
				CoinID:       target.ID,
				Coin:         target,
				ExSymbol:     data.RootSymbol,
				ChainType:    exchange.MAINNET,
				TxFee:        DEFAULT_TXFEE,
				Withdraw:     DEFAULT_WITHDRAW,
				Deposit:      DEFAULT_DEPOSIT,
				Confirmation: DEFAULT_CONFIRMATION,
				Listed:       true,
			}

			e.SetCoinConstraint(coinConstraint)
		}
	}
	return nil
}

/* GetPairsData - Get Pairs Information (If API provide)
Step 1: Change Instance Name    (e *<exchange Instance Name>)
Step 2: Add Model of API Response
Step 3: Modify API Path(strRequestUrl)*/
func (e *Bitmex) GetPairsData() error {
	pairsData := &PairsData{}

	strRequestUrl := "/api/v1/instrument/active"
	strUrl := API_URL + strRequestUrl

	jsonSymbolsReturn := exchange.HttpGetRequest(strUrl, nil)
	if err := json.Unmarshal([]byte(jsonSymbolsReturn), &pairsData); err != nil {
		return fmt.Errorf("%s Get Pairs Json Unmarshal Err: %v %v", e.GetName(), err, jsonSymbolsReturn)
	}

	for _, data := range *pairsData {
		if (data.QuoteCurrency == "USD" && data.Typ == "FFWCSX") || data.QuoteCurrency != "USD" {
			p := &pair.Pair{}
			switch e.Source {
			case exchange.EXCHANGE_API:
				base := coin.GetCoin(data.QuoteCurrency)
				target := coin.GetCoin(data.RootSymbol)
				if base != nil && target != nil && base != target {
					p = pair.GetPair(base, target)
				}
			case exchange.JSON_FILE:
				p = e.GetPairBySymbol(data.Symbol)
			}

			if p != nil {
				pairConstraint := &exchange.PairConstraint{
					PairID:      p.ID,
					Pair:        p,
					ExSymbol:    data.Symbol,
					MakerFee:    data.MakerFee,
					TakerFee:    data.TakerFee,
					LotSize:     data.LotSize,
					PriceFilter: data.TickSize,
					Listed:      true,
				}
				e.SetPairConstraint(pairConstraint)
			}
		}
	}
	return nil
}

/*Get Pair Market Depth
Step 1: Change Instance Name    (e *<exchange Instance Name>)
Step 2: Add Model of API Response
Step 3: Get Exchange Pair Code ex. symbol := e.GetPairCode(p)
Step 4: Modify API Path(strRequestUrl)
Step 5: Add Params - Depend on API request
Step 6: Convert the response to Standard Maker struct*/
func (e *Bitmex) OrderBook(p *pair.Pair) (*exchange.Maker, error) {
	errResponse := ErrorResponse{}
	orderBook := OrderBook{}

	mapParams := make(map[string]string)
	mapParams["symbol"] = e.GetSymbolByPair(p)
	mapParams["depth"] = "0"

	strRequestUrl := "/api/v1/orderBook/L2"
	strUrl := API_URL + strRequestUrl

	maker := &exchange.Maker{
		WorkerIP:        exchange.GetExternalIP(),
		Source:          exchange.EXCHANGE_API,
		BeforeTimestamp: float64(time.Now().UnixNano() / 1e6),
	}

	jsonOrderbook := exchange.HttpGetRequest(strUrl, mapParams)
	if err := json.Unmarshal([]byte(jsonOrderbook), &orderBook); err != nil {
		if err := json.Unmarshal([]byte(jsonOrderbook), &errResponse); err != nil {
			return nil, fmt.Errorf("%s OrderBook Unmarshal Err: %v %v", e.GetName(), err, jsonOrderbook)
		} else {
			return nil, fmt.Errorf("%s Get OrderBook Failed: %v %v", e.GetName(), errResponse.Error.Name, errResponse.Error.Message)
		}
	}

	maker.AfterTimestamp = float64(time.Now().UnixNano() / 1e6)

	for _, bid := range orderBook {
		if bid.Side == "Buy" {
			buydata := exchange.Order{}

			buydata.Rate = bid.Price
			buydata.Quantity = bid.Size / bid.Price

			maker.Bids = append(maker.Bids, buydata)
		}
	}
	for i := len(orderBook) - 1; i >= 0; i-- {
		if orderBook[i].Side == "Sell" {
			selldata := exchange.Order{}

			selldata.Rate = orderBook[i].Price
			selldata.Quantity = orderBook[i].Size / orderBook[i].Price

			maker.Asks = append(maker.Asks, selldata)
		}
	}

	return maker, nil
}

/*************** Private API ***************/
func (e *Bitmex) UpdateAllBalances() {
	if e.API_KEY == "" || e.API_SECRET == "" {
		log.Printf("%s API Key or Secret Key are nil.", e.GetName())
		return
	}

	accountBalance := AccountBalances{}
	errResponse := ErrorResponse{}
	strRequest := "/api/v1/user/wallet"

	mapParams := make(map[string]string)

	jsonBalanceReturn := e.ApiKeyGet(mapParams, strRequest)
	// log.Printf("jsonBalanceReturn: %v", jsonBalanceReturn)
	if err := json.Unmarshal([]byte(jsonBalanceReturn), &accountBalance); err != nil {
		if err := json.Unmarshal([]byte(jsonBalanceReturn), &errResponse); err != nil {
			log.Printf("%s Get Balance Unmarshal Err: %v %s", e.GetName(), err, jsonBalanceReturn)
		} else {
			log.Printf("%s Get Balance Failed: %s %s", e.GetName(), errResponse.Error.Name, errResponse.Error.Message)
		}
	}
	if strings.Contains(jsonBalanceReturn, "error") {
		log.Printf("%s Get Balance Failed: %s", e.GetName(), jsonBalanceReturn)
	}

	c := e.GetCoinBySymbol(strings.ToUpper(accountBalance.Currency))
	if c != nil {
		amount := accountBalance.Amount / 100000000.0
		balanceMap.Set(c.Code, amount)
	}
}

// use tag as 2FA token, quantity*100000000 to be satoshi amount
func (e *Bitmex) Withdraw(coin *coin.Coin, quantity float64, addr, tag string) bool {
	if e.API_KEY == "" || e.API_SECRET == "" {
		log.Printf("%s API Key or Secret Key are nil.", e.GetName())
		return false
	}

	satoshiQuantity := quantity * 100000000

	errResponse := ErrorResponse{}
	withdraw := Withdraw{}
	strRequest := "/api/v1/user/requestWithdrawal"

	mapParams := make(map[string]string)
	mapParams["currency"] = "XBt" //coin.Code // only one option: XBt
	mapParams["amount"] = fmt.Sprintf("%v", satoshiQuantity)
	mapParams["address"] = addr
	mapParams["otpToken"] = tag //2FA token

	jsonWithdrawReturn := e.ApiKeyPost(mapParams, strRequest)
	// log.Printf("jsonWithdrawReturn: %v", jsonWithdrawReturn) //=============================================
	if err := json.Unmarshal([]byte(jsonWithdrawReturn), &withdraw); err != nil {
		if err := json.Unmarshal([]byte(jsonWithdrawReturn), &errResponse); err != nil {
			log.Printf("%s Withdraw Unmarshal Err: %v %s", e.GetName(), err, jsonWithdrawReturn)
			return false
		} else {
			log.Printf("%s Withdraw Failed: %v %s", e.GetName(), errResponse.Error.Name, errResponse.Error.Message)
			return false
		}
	} else {
		if strings.Contains(jsonWithdrawReturn, "error") {
			log.Printf("%s Withdraw Failed: %s", e.GetName(), jsonWithdrawReturn)
			return false
		}

		return true
	}

	return false
}

func (e *Bitmex) LimitSell(pair *pair.Pair, quantity, rate float64) (*exchange.Order, error) {
	if e.API_KEY == "" || e.API_SECRET == "" {
		return nil, fmt.Errorf("%s API Key or Secret Key are nil.", e.GetName())
	}

	// errResponse := ErrorResponse{}
	// placeOrder := PlaceOrder{}
	// strRequest := "/api/v1/order"

	// mapParams := make(map[string]string)
	// mapParams["symbol"] = e.GetSymbolByPair(pair)
	// mapParams["side"] = "Sell"
	// mapParams["orderQty"] = strconv.FormatFloat(quantity, 'f', -1, 64) //simpleOrderQty
	// mapParams["price"] = strconv.FormatFloat(rate, 'f', -1, 64)

	// jsonPlaceReturn := e.ApiKeyPost(mapParams, strRequest)
	// if err := json.Unmarshal([]byte(jsonPlaceReturn), &placeOrder); err != nil {
	// 	if err := json.Unmarshal([]byte(jsonPlaceReturn), &errResponse); err != nil {
	// 		return nil, fmt.Errorf("%s LimitSell Unmarshal Err: %v %v", e.GetName(), err, jsonPlaceReturn)
	// 	} else {
	// 		return nil, fmt.Errorf("%s Place LimitSell Failed: %v %v", e.GetName(), errResponse.Error.Name, errResponse.Error.Message)
	// 	}
	// } else {
	// 	order := &exchange.Order{
	// 		Pair:         pair,
	// 		Side:         "Sell",
	// 		OrderID:      placeOrder.OrderID,
	// 		Rate:         rate,
	// 		Quantity:     quantity,
	// 		Status:       exchange.New,
	// 		JsonResponse: jsonPlaceReturn,
	// 	}
	// 	return order, nil
	// }
	return nil, nil
}

func (e *Bitmex) LimitBuy(pair *pair.Pair, quantity, rate float64) (*exchange.Order, error) {
	if e.API_KEY == "" || e.API_SECRET == "" {
		return nil, fmt.Errorf("%s API Key or Secret Key are nil.", e.GetName())
	}

	// errResponse := ErrorResponse{}
	// placeOrder := PlaceOrder{}
	// strRequest := "/api/v1/order"

	// mapParams := make(map[string]string)
	// mapParams["symbol"] = e.GetSymbolByPair(pair)
	// mapParams["side"] = "Buy"
	// mapParams["orderQty"] = strconv.FormatFloat(quantity, 'f', -1, 64) //simpleOrderQty
	// mapParams["price"] = strconv.FormatFloat(rate, 'f', -1, 64)

	// jsonPlaceReturn := e.ApiKeyPost(mapParams, strRequest)
	// if err := json.Unmarshal([]byte(jsonPlaceReturn), &placeOrder); err != nil {
	// 	if err := json.Unmarshal([]byte(jsonPlaceReturn), &errResponse); err != nil {
	// 		return nil, fmt.Errorf("%s LimitSell Unmarshal Err: %v %s", e.GetName(), err, jsonPlaceReturn)
	// 	} else {
	// 		return nil, fmt.Errorf("%s Place LimitSell Failed: %v %v", e.GetName(), errResponse.Error.Name, errResponse.Error.Message)
	// 	}
	// } else {
	// 	order := &exchange.Order{
	// 		Pair:         pair,
	// 		Side:         "Buy",
	// 		OrderID:      placeOrder.OrderID,
	// 		Rate:         rate,
	// 		Quantity:     quantity,
	// 		Status:       exchange.New,
	// 		JsonResponse: jsonPlaceReturn,
	// 	}
	// 	return order, nil
	// }
	return nil, nil
}

func (e *Bitmex) OrderStatus(order *exchange.Order) error {
	if e.API_KEY == "" || e.API_SECRET == "" {
		return fmt.Errorf("%s API Key or Secret Key are nil.", e.GetName())
	}

	errResponse := ErrorResponse{}
	orderStatus := []PlaceOrder{}
	strRequest := "/api/v1/order"

	mapParams := make(map[string]string)
	// mapParams["filter"] = fmt.Sprintf(`{"orderID": "%s"}`, "49da8c8c-6f40-58d4-a4b8-60e7f29c285e" /* order.OrderID */)

	jsonOrderStatus := e.ApiKeyGet(mapParams, strRequest)
	if err := json.Unmarshal([]byte(jsonOrderStatus), &orderStatus); err != nil {
		if err := json.Unmarshal([]byte(jsonOrderStatus), &errResponse); err != nil {
			return fmt.Errorf("%s OrderStatus Unmarshal Err: %v %s", e.GetName(), err, jsonOrderStatus)
		} else {
			return fmt.Errorf("%s Get OrderStatus Failed: %v %s", e.GetName(), errResponse.Error.Name, errResponse.Error.Message)
		}
	} else {
		if strings.Contains(jsonOrderStatus, "error") {
			return fmt.Errorf("%s OrderStatus Failed: %v", e.GetName(), jsonOrderStatus)
		} else if len(orderStatus) == 0 {
			return fmt.Errorf("%s OrderStatus order not found: %v, pair: %v", e.GetName(), order.OrderID, order.Pair.Name)
		}
		for _, orderStatus := range orderStatus {
			if orderStatus.OrderID == order.OrderID {
				if orderStatus.OrdStatus == "Filled" {
					order.Status = exchange.Filled
				} else if orderStatus.OrdStatus == "Canceled" {
					order.Status = exchange.Canceled
				} else if orderStatus.OrdStatus == "Partial" {
					order.Status = exchange.Partial
				} else {
					order.Status = exchange.Other
				}
				order.DealRate = orderStatus.AvgPx
				order.DealQuantity = orderStatus.SimpleOrderQty - orderStatus.SimpleLeavesQty

				return nil
			}
		}
	}

	return fmt.Errorf("%s Could not find Order: %v", e.GetName(), order.OrderID)
}

func (e *Bitmex) ListOrders() ([]*exchange.Order, error) {
	return nil, nil
}

func (e *Bitmex) CancelOrder(order *exchange.Order) error {
	if e.API_KEY == "" || e.API_SECRET == "" {
		return fmt.Errorf("%s API Key or Secret Key are nil.", e.GetName())
	}

	return nil
}

func (e *Bitmex) CancelAllOrder() error {
	return nil
}

func (e *Bitmex) MarginAction(data *exchange.Margin) error {
	return nil
}

func (e *Bitmex) ContractAction(data *exchange.Contract) error {
	if e.API_KEY == "" || e.API_SECRET == "" {
		return fmt.Errorf("%s API Key or Secret Key are nil.", e.GetName())
	}

	switch data.Action {
	case exchange.CONTRACT_MARKET_BUY, exchange.CONTRACT_MARKET_SELL, exchange.CONTRACT_LIMIT_BUY, exchange.CONTRACT_LIMIT_SELL:
		return e.contractPlaceOrder(data)
	case exchange.GET_ADDR:
		return e.getAddr(data)
	case exchange.LIQUIDATION: // need test
		return e.getLiquidation(data)
	case exchange.CONTRACT_ORDER_STATUS:
		return e.contractOrderStatus(data)
	case exchange.CONTRACT_BALANCE:
		return e.contractBalances(data)
	}

	return nil
}

func (e *Bitmex) contractBalances(data *exchange.Contract) error {
	// accountBalance := AccountBalances{}
	accountBalance := MarginBalances{}
	errResponse := ErrorResponse{}
	strRequest := "/api/v1/user/margin"

	jsonBalanceReturn := e.ApiKeyGet(nil, strRequest)
	// log.Printf("===============jsonBalanceReturn: %v", jsonBalanceReturn) // ==============
	if err := json.Unmarshal([]byte(jsonBalanceReturn), &accountBalance); err != nil {
		if err := json.Unmarshal([]byte(jsonBalanceReturn), &errResponse); err != nil {
			return fmt.Errorf("%s ContractBalances Unmarshal Err: %v %s", e.GetName(), err, jsonBalanceReturn)
		} else {
			return fmt.Errorf("%s ContractBalances Failed: %s %s", e.GetName(), errResponse.Error.Name, errResponse.Error.Message)
		}
	}
	if strings.Contains(jsonBalanceReturn, "error") {
		return fmt.Errorf("%s ContractBalances Failed: %s", e.GetName(), jsonBalanceReturn)
	}

	// Write into return struct
	data.ContractBalanceReturn = &exchange.ContractBalance{
		MarginBalance:     accountBalance.MarginBalance / 100000000,
		MarginAvailable:   accountBalance.AvailableMargin / 100000000,
		WithdrawAvailable: accountBalance.WithdrawableMargin / 100000000,
	}

	return nil
}

func (e *Bitmex) setLeverage(pair *pair.Pair, leverage float64) error {
	errResponse := ErrorResponse{}
	setLeverage := &SetLeverage{}
	strRequest := "/api/v1/position/leverage"

	mapParams := make(map[string]string)
	mapParams["symbol"] = e.GetSymbolByPair(pair)
	mapParams["leverage"] = fmt.Sprintf("%.2f", leverage)

	jsonLeverageReturn := e.ApiKeyPost(mapParams, strRequest)
	// log.Printf("jsonLeverageReturn: %v", jsonLeverageReturn) // ==================
	if err := json.Unmarshal([]byte(jsonLeverageReturn), &setLeverage); err != nil {
		log.Printf("%s Set Leverage Err: %+v %s", e.GetName(), err, jsonLeverageReturn)
		if err := json.Unmarshal([]byte(jsonLeverageReturn), &errResponse); err != nil {
			return fmt.Errorf("%s Leverage Unmarshal Err: %v %s", e.GetName(), err, jsonLeverageReturn)
		} else {
			return fmt.Errorf("%s Leverage Unmarshal Err: %+v %s", e.GetName(), errResponse.Error, jsonLeverageReturn)
		}
	} else {
		if strings.Contains(jsonLeverageReturn, "error") {
			return fmt.Errorf("%s Place Set Leverage Failed: %s", e.GetName(), jsonLeverageReturn)
		}
	}

	return nil
}

// orderType: "Limit" or "Market"
func (e *Bitmex) contractPlaceOrder(data *exchange.Contract) error {
	err := e.setLeverage(data.Pair, data.Leverage)
	if err != nil {
		return fmt.Errorf("%s %s set Leverage Err: %+v, Leverage: %f", e.GetName(), data.Action, err, data.Leverage)
	}

	// satoshiQuantity := data.Quantity * 100000000

	errResponse := ErrorResponse{}
	placeOrder := PlaceOrder{}
	strRequest := "/api/v1/order"

	priceFilter := int(math.Round(math.Log10(e.GetPriceFilter(data.Pair)) * -1))
	lotSize := int(math.Round(math.Log10(e.GetLotSize(data.Pair)) * -1))

	mapParams := make(map[string]string)
	mapParams["symbol"] = e.GetSymbolByPair(data.Pair)
	switch data.Action {
	case exchange.CONTRACT_MARKET_BUY:
		mapParams["ordType"] = "Market"
		mapParams["side"] = "Buy"
	case exchange.CONTRACT_MARKET_SELL:
		mapParams["ordType"] = "Market"
		mapParams["side"] = "Sell"
	case exchange.CONTRACT_LIMIT_BUY:
		mapParams["ordType"] = "Limit"
		mapParams["side"] = "Buy"
	case exchange.CONTRACT_LIMIT_SELL:
		mapParams["ordType"] = "Limit"
		mapParams["side"] = "Sell"
	}
	mapParams["orderQty"] = strconv.FormatFloat(data.Quantity, 'f', lotSize, 64)
	if data.Rate != 0 {
		mapParams["price"] = strconv.FormatFloat(data.Rate, 'f', priceFilter, 64)
	}

	jsonPlaceReturn := e.ApiKeyPost(mapParams, strRequest)
	// log.Printf("jsonPlaceReturn: %v=%v", jsonPlaceReturn, strings.Contains(jsonPlaceReturn, "error"))
	if err := json.Unmarshal([]byte(jsonPlaceReturn), &placeOrder); err != nil {
		if err := json.Unmarshal([]byte(jsonPlaceReturn), &errResponse); err != nil {
			return fmt.Errorf("%s %s Unmarshal Err: %s, %s", e.GetName(), data.Action, err, jsonPlaceReturn)
		} else {
			return fmt.Errorf("%s %s Failed: %s, %s", e.GetName(), data.Action, errResponse.Error.Name, errResponse.Error.Message)
		}
	} else {
		if strings.Contains(jsonPlaceReturn, "error") {
			return fmt.Errorf("%s %s Failed: %s", e.GetName(), data.Action, jsonPlaceReturn)
		}

		// Write into return struct
		data.Order = &exchange.Order{
			Pair:      data.Pair,
			OrderID:   placeOrder.OrderID,
			Rate:      data.Rate,
			Quantity:  data.Quantity,
			Side:      mapParams["side"],
			OrderType: mapParams["ordType"],
			Leverage:  data.Leverage,
			Status:    exchange.New,
		}

		return nil
	}
}

func (e *Bitmex) contractOrderStatus(data *exchange.Contract) error {
	errResponse := ErrorResponse{}
	orderStatus := []*PlaceOrder{}
	strRequest := "/api/v1/order"

	mapParams := make(map[string]string)

	filterMap := make(map[string]string)
	filterMap["orderID"] = data.Order.OrderID //"a7e80f55-3c88-b08b-a63a-2b7b4bdd6f03"
	filter, err := json.Marshal(filterMap)
	if err != nil {
		log.Printf("marshal err: %v", err)
	}
	if data.Order.OrderID != "" {
		mapParams["filter"] = string(filter)
	}

	jsonOrderStatus := e.ApiKeyGet(mapParams, strRequest)
	// log.Printf("jsonOrderStatus: %v", jsonOrderStatus)
	if err := json.Unmarshal([]byte(jsonOrderStatus), &orderStatus); err != nil {
		if err := json.Unmarshal([]byte(jsonOrderStatus), &errResponse); err != nil {
			return fmt.Errorf("%s OrderStatus Unmarshal Err: %v %s", e.GetName(), err, jsonOrderStatus)
		} else {
			return fmt.Errorf("%s Get OrderStatus Failed: %s %s", e.GetName(), errResponse.Error.Name, errResponse.Error.Message)
		}
	} else {
		if strings.Contains(jsonOrderStatus, "error") {
			return fmt.Errorf("%s OrderStatus Failed: %s", e.GetName(), jsonOrderStatus)
		} else if len(orderStatus) == 0 {
			return fmt.Errorf("%s OrderStatus order not found: %v", e.GetName(), data.Order.OrderID)
		}

		for _, order := range orderStatus {
			if order.OrderID != data.Order.OrderID {
				continue
			}

			// calculate from satoshi to normal
			// order.OrderQty = order.OrderQty / 100000000.0

			if order.OrderID == data.Order.OrderID {
				if order.OrdStatus == "Filled" {
					data.Order.Status = exchange.Filled
				} else if order.OrdStatus == "Canceled" {
					data.Order.Status = exchange.Canceled
				} else if order.OrdStatus == "Partial" {
					data.Order.Status = exchange.Partial
				} else {
					data.Order.Status = exchange.Other
				}

				data.Order.DealRate = order.AvgPx
				data.Order.DealQuantity = order.CumQty
			}

			return nil
		}
	}

	return fmt.Errorf("%s Could not find Order: %v", e.GetName(), data.Order.OrderID)
}

func (e *Bitmex) getAddr(data *exchange.Contract) error {
	errResponse := ErrorResponse{}
	accountInfo := AccountInfo{}
	strRequest := "/api/v1/user/wallet"

	mapParams := make(map[string]string)
	mapParams["currency"] = "XBt" // symbol here, eg: XBt,  data.Currency.Code, only one option: XBt

	jsonAddrReturn := e.ApiKeyGet(mapParams, strRequest)
	if err := json.Unmarshal([]byte(jsonAddrReturn), &accountInfo); err != nil {
		if err := json.Unmarshal([]byte(jsonAddrReturn), &errResponse); err != nil {
			return fmt.Errorf("%s GetAddr Unmarshal Err: %v, %s", e.GetName(), err, jsonAddrReturn)
		} else {
			return fmt.Errorf("%s GetAddr Failed: %s, %s", e.GetName(), errResponse.Error.Name, errResponse.Error.Message)
		}
	} else {
		if strings.Contains(jsonAddrReturn, "error") {
			return fmt.Errorf("%s GetAddr Failed: %s", e.GetName(), jsonAddrReturn)
		}
		data.Addr = accountInfo.Addr

		return nil
	}
}

func (e *Bitmex) getLiquidation(data *exchange.Contract) error {
	errResponse := ErrorResponse{}
	getLiquidation := []*exchange.LiquidationOrder{}
	strRequest := "/api/v1/liquidation"

	mapParams := make(map[string]string)
	mapParams["symbol"] = e.GetSymbolByCoin(data.Currency) // XBT

	jsonLiquidationReturn := e.ApiKeyGet(mapParams, strRequest)
	// log.Printf("jsonLiquidationReturn: %v, err? %v", jsonLiquidationReturn, strings.Contains(jsonLiquidationReturn, "error"))
	if err := json.Unmarshal([]byte(jsonLiquidationReturn), &getLiquidation); err != nil {
		if err := json.Unmarshal([]byte(jsonLiquidationReturn), &errResponse); err != nil {
			return fmt.Errorf("%s getLiquidation Unmarshal Err: %v, %s", e.GetName(), err, jsonLiquidationReturn)
		} else {
			return fmt.Errorf("%s getLiquidation Failed: %s, %s", e.GetName(), errResponse.Error.Name, errResponse.Error.Message)
		}
	} else {
		if strings.Contains(jsonLiquidationReturn, "error") {
			return fmt.Errorf("%s getLiquidation Failed: %s", e.GetName(), jsonLiquidationReturn)
		}

		data.LiquidationOrders = getLiquidation
		return nil
	}
}

/*************** Signature Http Request ***************/
/*Method: GET and Signature is required  --reference Binance
Step 1: Change Instance Name    (e *<exchange Instance Name>)
Step 2: Create mapParams Depend on API Signature request
Step 3: Add HttpGetRequest below strUrl if API has different requests*/
func (e *Bitmex) ApiKeyGet(mapParams map[string]string, strRequestPath string) string {
	strMethod := "GET"
	timestamp := time.Now().Unix() + 5

	var strRequestUrl string
	if mapParams == nil || len(mapParams) == 0 {
		strRequestUrl = strRequestPath
	} else {
		strParams := exchange.Map2UrlQuery(mapParams)
		strRequestUrl = strRequestPath + "?" + strParams
	}

	timestamp10, _ := strconv.Atoi(strconv.FormatInt(timestamp, 10))
	strPayload := fmt.Sprintf("%s%s%d", strMethod, strRequestUrl, timestamp10)

	mapParams2Sign := make(map[string]string)
	mapParams2Sign["api-expires"] = strconv.FormatInt(timestamp, 10)
	mapParams2Sign["api-key"] = e.API_KEY
	mapParams2Sign["api-signature"] = exchange.ComputeHmac256NoDecode(strPayload, e.API_SECRET)

	strUrl := API_URL + strRequestUrl

	httpClient := &http.Client{}

	// 构建Request, 并且按官方要求添加Http Header
	request, err := http.NewRequest(strMethod, strUrl, nil)
	if nil != err {
		return err.Error()
	}

	request.Header.Add("Content-Type", "application/x-www-form-urlencoded")
	request.Header.Add("Accept", "application/json")
	request.Header.Add("Accept", "application/json")
	request.Header.Add("api-expires", strconv.FormatInt(timestamp, 10))
	request.Header.Add("api-key", e.API_KEY)

	request.Header.Add("api-signature", exchange.ComputeHmac256NoDecode(strPayload, e.API_SECRET))

	// 发出请求
	response, err := httpClient.Do(request)
	if nil != err {
		return err.Error()
	}
	defer response.Body.Close()

	// 解析响应内容
	body, err := ioutil.ReadAll(response.Body)
	if nil != err {
		return err.Error()
	}

	return string(body)
}

/*Method: API Request and Signature is required
Step 1: Change Instance Name    (e *<exchange Instance Name>)
Step 2: Create mapParams Depend on API Signature request
Step 3: Add HttpGetRequest below strUrl if API has different requests*/
func (e *Bitmex) ApiKeyPost(mapParams map[string]string, strRequestPath string) string {
	strMethod := "POST"
	timestamp := time.Now().Unix() + 5

	jsonParams := ""
	lenByte := 0
	if nil != mapParams {
		bytesParams, _ := json.Marshal(mapParams)
		jsonParams = string(bytesParams)
		lenByte = len(bytesParams)
	}
	strUrl := API_URL + strRequestPath
	httpClient := &http.Client{}

	// 构建Request, 并且按官方要求添加Http Header
	request, err := http.NewRequest(strMethod, strUrl, strings.NewReader(jsonParams))
	if nil != err {
		return err.Error()
	}

	request.Header.Add("Content-Length", fmt.Sprintf("%d", lenByte))
	request.Header.Add("Content-Type", "application/json")
	request.Header.Add("Accept", "application/json")
	request.Header.Add("api-expires", strconv.FormatInt(timestamp, 10))
	request.Header.Add("api-key", e.API_KEY)

	timestamp10, _ := strconv.Atoi(strconv.FormatInt(timestamp, 10))
	strPayload := fmt.Sprintf("%s%s%d%s", strMethod, strRequestPath, timestamp10, jsonParams)

	request.Header.Add("api-signature", exchange.ComputeHmac256NoDecode(strPayload, e.API_SECRET))

	// 发出请求
	response, err := httpClient.Do(request)
	if nil != err {
		return err.Error()
	}
	defer response.Body.Close()

	// 解析响应内容
	body, err := ioutil.ReadAll(response.Body)
	if nil != err {
		return err.Error()
	}

	// ************************
	// // apiKey := "LAqUlngMIQkIUjXMUreyu3qn"
	// apiSecret := "chNOOS4KvNXR_Xq4k4c9qsfoKWvnDecLATCRlcBwyKDYnWgO"
	// verb := "GET"
	// path := "/api/v1/instrument"
	// expires := 1518064236
	// data := ``
	// testPayload := fmt.Sprintf("%s%s%d%s", verb, path, expires, data)
	// log.Printf("Signature!!!: %v", exchange.ComputeHmac256NoDecode(testPayload, apiSecret))

	// ************************
	// apiKey := "LAqUlngMIQkIUjXMUreyu3qn"
	// apiSecret := "chNOOS4KvNXR_Xq4k4c9qsfoKWvnDecLATCRlcBwyKDYnWgO"
	// verb := "GET"
	// path := "/api/v1/instrument?filter=%7B%22symbol%22%3A+%22XBTM15%22%7D"
	// expires := 1518064237
	// data := ``
	// testPayload := fmt.Sprintf("%s%s%d%s", verb, path, expires, data)
	// log.Printf("Signature!!!: %v", exchange.ComputeHmac256NoDecode(testPayload, apiSecret))

	// ************************
	// // apiKey := "LAqUlngMIQkIUjXMUreyu3qn"
	// apiSecret := "chNOOS4KvNXR_Xq4k4c9qsfoKWvnDecLATCRlcBwyKDYnWgO"
	// verb := "POST"
	// path := "/api/v1/order"
	// expires := 1518064238
	// data := `{"symbol":"XBTM15","price":219.0,"clOrdID":"mm_bitmex_1a/oemUeQ4CAJZgP3fjHsA","orderQty":98}`
	// testPayload := fmt.Sprintf("%s%s%d%s", verb, path, expires, data)
	// log.Printf("Signature!!!: %v", exchange.ComputeHmac256NoDecode(testPayload, apiSecret))

	return string(body)
}
